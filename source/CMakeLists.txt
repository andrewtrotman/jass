#
# CMAKELISTS.TXT
# --------------
#
# Build file for JASS
# Copyright (c) 2016 Andrew Trotman
#
# Released under the 2-clause BSD license (See:https://en.wikipedia.org/wiki/BSD_licenses)
#

set(JASSlib_FILES
	allocator.h
	allocator_cpp.h
	allocator_memory.h
	allocator_memory.cpp
	allocator_pool.h
	allocator_pool.cpp
	ascii.h
	ascii.cpp
	asserts.h
	asserts.cpp
	binary_tree.h
	bitstring.h
	bitstring.cpp
	checksum.h
	checksum.cpp
	compress_integer.h
	compress_integer_variable_byte.h
	compress_integer_variable_byte.cpp
	document.h
	dynamic_array.h
	file.h
	file.cpp
	hash_table.h
	hash_pearson.h
	hash_pearson.cpp
	index_manager.h
	index_manager_sequential.h
	index_postings.h
	instream.h
	instream_document_trec.h
	instream_document_trec.cpp
	instream_file.h
	instream_file.cpp
	instream_memory.h
	instream_memory.cpp
	maths.h
	parser.h
	parser.cpp
	serialise_jass_ci.cpp
	serialise_jass_ci.h
	slice.h
	unicode.h
	unicode.cpp
	unittest_data.h
	unittest_data.cpp
	version.h
	)

add_library(JASSlib ${JASSlib_FILES})

include_directories(.)

#
# Tell cmake about the source code files that are generated by other parts of the build process.
#
set_source_files_properties(unicode.cpp PROPERTIES GENERATED 1)
add_dependencies(JASSlib generate_unicode_cpp)

set_source_files_properties(ascii.cpp PROPERTIES GENERATED 1)
add_dependencies(JASSlib generate_ascii_cpp)

source_group ("Source Files" FILES ${JASSlib_FILES})
