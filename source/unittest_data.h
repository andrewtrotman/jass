/*
	UNITTEST_DATA.H
	---------------
	Copyright (c) 2016 Andrew Trotman
	Released under the 2-clause BSD license (See:https://en.wikipedia.org/wiki/BSD_licenses)
*/
/*!
	@file
	@brief Data that can be used and re-used for unittests (and other purposes).
	@author Andrew Trotman
	@copyright 2016 Andrew Trotman
*/

#include <string>

namespace JASS
	{
	/*!
		@brief Global data used for unit testing.
	*/
	class unittest_data
		{
		public:
			static std::string ten_documents;		///< Ten TREC formatted documents with ten terms (ten .. one) where each term occurs it's count number of times.
			static std::string ten_document_1;	///< The first of the 10 ten_documents.
			static std::string ten_document_2;	///< The second of the 10 ten_documents.
			static std::string ten_document_3;	///< The third of the 10 ten_documents.
			static std::string ten_document_4;	///< The fourth of the 10 ten_documents.
			static std::string ten_document_5;	///< The fifth of the 10 ten_documents.
			static std::string ten_document_6;	///< The sixth of the 10 ten_documents.
			static std::string ten_document_7;	///< The seventh of the 10 ten_documents.
			static std::string ten_document_8;	///< The eightth of the 10 ten_documents.
			static std::string ten_document_9;	///< The ninth of the 10 ten_documents.
			static std::string ten_document_10;	///< The tenth of the 10 ten_documents.
		};
	}
